import numpy as np
import cv2
import Config
import json
from datetime import datetime


def resizing(img):
    """
    TODO optional
    Resolution of video / video stream of ...(GoPro Dupe) is 1920 width x 1080 height
    """
    return cv2.resize(img, (1920, 1080), interpolation=cv2.INTER_AREA)

def get_log_id():
    """
    grabs persistent current log id
    :return: current log id
    """
    with open("log_id.txt", "r") as f:
        LOG_ID = f.read()
    f.close()
    # update log id for next session
    NEW_LOG_ID = int(LOG_ID) + 1
    with open("log_id.txt", "w") as new_file:
        new_file.write(str(NEW_LOG_ID))
    f.close()
    return str(LOG_ID)

def code_direction(direction):
    """
    codes direction vector into easier format that represents abstract direction
    [up, down, left, right] with 0 or 1
    """

    angle = np.degrees(np.arctan2(direction[1], direction[0]))
    directionRepr = [0, 0, 0, 0]
    ovlp = 10 # by how many degrees the directions [up, down, left, right] can overlap

    # exchange up and down since coordinate system is mirrored horizontically
    # classifies as down
    if angle >= 45-ovlp and angle <= 135+ovlp:
        directionRepr[1] = 1
    # classifies as up
    elif angle <= -45-ovlp and angle >= -145-ovlp:
        directionRepr[0] = 1
    # classifies as left
    if (angle >= 135+ovlp and angle <= 180) or (angle <= -135-ovlp and angle >= -180):
        directionRepr[2] = 1
    # classifies as right
    elif (angle >= 0 and angle <= 45+ovlp) or (angle <= 0 and angle >= -45-ovlp):
        directionRepr[3] = 1

    pass
    #return directionRepr


def publish_update(message):
    # TODO
    pass
