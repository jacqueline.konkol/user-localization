import cv2
import numpy as np

cap = cv2.VideoCapture(1)

while True:
    timer = cv2.getTickCount()
    success, img = cap.read()

    cv2.imshow('stream', img)

    if cv2.waitKey(1) & 0xff == ord('q'):
        break
