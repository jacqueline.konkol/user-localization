broker_address = "192.168.178.29" 

segmentPath = "prepared.json" # else put None
#segmentPath = None

#videoPath = 'stream'
videoPath = 'samples/2.avi'
#videoPath = 'samples/3.avi'
#videoPath = 'samples/1.avi'
#videoPath = 'samples/richtige_Ausrichtung.avi'
#videoPath = 'samples/lab_only_human.avi'
#videoPath = 'samples/stairs.avi'
#videoPath = 'samples/shoes.avi'
#videoPath = 'samples/robot_and_human.avi'


# enable live-display which will significantly decrease performance

#display = True
display = False

### CALIBRATION SETTINGS FOR DIFFERENT SCENES

# add custom calibration settings to fit your scene and name them in a unique way (e.g. 'stream-lights-on')

#calibration = 'samples/2.avi'
calibration = 'stream'

if calibration == 'samples/richtige_Ausrichtung.avi' or calibration == 'samples/2.avi' or calibration == 'samples/3.avi':
    blur_kernel = (49,49)
    diff_threshold = 10
    human_contour_area = 17000
    threshWhite = 220

elif calibration == 'samples/lab_only_human.avi':
    blur_kernel = (29,29)
    diff_threshold = 10
    human_contour_area = 5000
    threshWhite = 230

elif calibration == 'samples/1.avi':
    blur_kernel = (19,19)
    diff_threshold = 10
    human_contour_area = 7000
    threshWhite = 230

elif calibration == 'samples/shoes.avi':
    blur_kernel = (49, 49)
    diff_threshold = 10
    human_contour_area = 50000
    threshWhite = 230

elif calibration == 'samples/robot_and_human.avi':
    blur_kernel = (69, 69)
    diff_threshold = 10
    human_contour_area = 50000
    threshWhite = 230 

# default settings
else:
    blur_kernel = (49, 49)
    diff_threshold = 15
    human_contour_area = 20000
    threshWhite = 220 # white is 255 on all channels. Threshold when it is considered white.
