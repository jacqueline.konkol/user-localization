import ipaaca
import paho.mqtt.client as mqtt
import time

import Config

def msg_to_ipaaca(msg):

    values = msg.split(',')
    ipaaca_msg = ipaaca.Message("HumanLocation")
    ipaaca_msg.payload = {
            'location': values[0],
            'time': values[1]
    }

# callback
def on_message(client, userdata, msg):
    print("message received " ,str(msg.payload.decode("utf-8")))
    print("message topic=",msg.topic)
    #print("message qos=",message.qos)
    msg_to_ipaaca(str(msg.payload.decode("utf-8")))

#setup client
client = mqtt.Client("LocationReceiver")
client.on_message=on_message #attach function to callback

print("connecting to broker")
client.connect(Config.broker_address) #connect to broker
client.loop_start() #start the loop
client.subscribe("Location")
time.sleep(60)
#client.loop_stop()

