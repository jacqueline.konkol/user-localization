# based on https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/

import numpy as np
import cv2
import Config
import json

if Config.videoPath == 'stream':
    cap = cv2.VideoCapture(0)
# reads from pre-recorded video
else:
    cap = cv2.VideoCapture(Config.videoPath)

_, img = cap.read() # the first frame is just black
_, img = cap.read()
segmentDict = {}

def name_segment(firstValues, secondValues):
    segment = input("What shall the current segment be named?")
    minValues = [firstValues[0] if firstValues[0] < secondValues[0] else secondValues[0],
                 firstValues[1] if firstValues[1] < secondValues[1] else secondValues[1]]
    maxValues = [firstValues[0] if firstValues[0] >= secondValues[0] else secondValues[0],
                 firstValues[1] if firstValues[1] >= secondValues[1] else secondValues[1]]
    print('Segment "' + segment + '" coordinates are ' + str(refPt[0]) + ' ' + str(refPt[1]))
    segmentDict[segment] = [minValues[0], maxValues[0], minValues[1], maxValues[1]]


def click_and_crop(event, x, y, flags, param):
    global refPt, cropping
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
    elif event == cv2.EVENT_LBUTTONUP:
        refPt.append((x, y))
        cv2.rectangle(img, refPt[0], refPt[1], (0, 255, 0), 2)
        name_segment(refPt[0], refPt[1])
        cv2.imshow("drag rectangle and name segment in terminal", img)

print("Drag a rectangle on the pop-up window to create a segment.")
print("Name that segment in the terminal, confirm with enter.")
print("The segment will appear green in the window.")
print("Proceed with as many segments as you wish.")
print("When done, press Q to finish.")
print("The segment information will be saved automatically.")
while (True):
    cv2.setMouseCallback("drag rectangle and name segment in terminal", click_and_crop)
    cv2.imshow("drag rectangle and name segment in terminal", img)
    if cv2.waitKey(20) & 0xFF == ord('q'):
        break

with open('segments/' + Config.videoPath.replace("samples/", "").replace(".avi", "") + '.json', 'w') as outfile:
    outfile.seek(0)
    json.dump(segmentDict, outfile)
    outfile.truncate()

cv2.destroyAllWindows()