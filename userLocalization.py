import cv2
import numpy as np
import datetime
import json
import time
import ipaaca
import paho.mqtt.client as mqtt

import Config
import tools

lastHumanBoundingBox = [0, 0, 0, 0]
lastPosition = "None"
LOG_ID = tools.get_log_id()

client = mqtt.Client("LocationPublisher")
client.connect(Config.broker_address) 
#outputBuffer = ipaaca.OutputBuffer('UserLocationSender')

# writing results into the log file
with open("logs/" + LOG_ID + ".txt","w") as log:

    class PositionBuffer():
        """
        determines most likely position and direction based on multiple datapoints
        more robust to noise
        """
        def __init__(self):
            self.max = 15 # over how many values the average is calculated
            self.maxCoo = 15 # over how many values the average is calculated
            self.positions = np.full(self.max, "", dtype="U25")
            self.coordinates = np.full((self.maxCoo, 2), 0, dtype=float)

        def update(self, value, centerx, centery):
            self.positions = np.roll(self.positions, -1)
            self.positions[self.max-1] = value
            self.coordinates = np.roll(self.coordinates, -2)
            self.coordinates[self.maxCoo-1, 0] = centerx
            self.coordinates[self.maxCoo-1, 1] = centery

        def avg(self):
            unique, pos = np.unique(self.positions, return_inverse=True)
            counts = np.bincount(pos)
            maxpos = counts.argmax()
            return unique[maxpos]

        def calc_direction(self):
            differences = np.diff(self.coordinates, axis=0)
            direction = np.sum(differences, axis=0)
            norm = np.linalg.norm(direction)
            if norm != 0:
                return direction / norm * 100
            else:
                return [0,0]


    def update_log(lastPosition, centerCoor=None):
        """
        during execution writes information into a persistent log file
        :param lastPosition: string
        :param centerCoor: list of x- and y-coordinates
        """
        dateTimeObj = datetime.datetime.now()
        dateTimeStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")
        global log

        # last segment, timestamp
        log.write(lastPosition + ": " + dateTimeStr + "\n")

        # last segment, exact coordinated of bounding box center, timestamp
        #log.write(lastPosition + " " + str(int(centerCoor[0])) + ", " + str(int(centerCoor[1])) + ": " + dateTimeStr + "\n")

        log.flush()

    def send_ipaaca_msg(lastPosition, centerCoor=None):
        """
        during execution sends information via ipaaca.Message
        :param lastPosition: string
        :param centerCoor: list of x- and y-coordinates
        """
        dateTimeObj = datetime.datetime.now()
        dateTimeStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")

        msg = ipaaca.Message(category='UserLocation')
        msg.payload = {
            'time': dateTimeStr,
            'location': lastPosition,
        }
        outputBuffer.add(msg)

    def send_mqtt_msg(lastPosition, centerCoor=None):

        dateTimeObj = datetime.datetime.now()
        dateTimeStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")

        msg = lastPosition + ',' + dateTimeStr
        client.publish("Location", msg)


    def dominant_color(frame, x, y, w, h):
        # calculate smaller bb inside of which the color should be analyzed

        x_new = x + int(w / 2 - w / 8)
        y_new = y + int(h / 2 - h / 8)

        bb_frame = frame[y_new:int(y_new+h/4), x_new:int(x_new+w/4)]
        bb_frame = np.float32(bb_frame.reshape(-1, 3))
        bb_frame = np.float32(bb_frame)
        n_colors = 1
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, .1)
        flags = cv2.KMEANS_RANDOM_CENTERS
        _, labels, palette = cv2.kmeans(bb_frame, n_colors, None, criteria, 10, flags)
        _, counts = np.unique(labels, return_counts=True)
        dominant = palette[np.argmax(counts)]

        return dominant

    def is_robot(frame1, x, y, w, h):
        """
        checks whether the object in bounding box exceeds the whiteness threshold, i.e. it is detected as a robot
        :param frame1:
        :param x: bounding box x-origin
        :param y: bounding box y-origin
        :param w: width of bounding box
        :param h: height of bounding box
        :return: whether the object is identified as a robot
        """
        dom_color = dominant_color(frame1, x, y, w, h)
        startx = x + int(w / 2 - w / 8)
        starty = y + int(h / 2 - h / 8)

        if Config.display:
            cv2.rectangle(frame1, pt1=(startx, starty), pt2=(startx + int(w / 4), starty + int(h / 4)),
                        color=(int(dom_color[0]), int(dom_color[1]), int(dom_color[2])),
                        thickness=2)
        # if inner bounding box is dominantly white
        if dom_color[0] > Config.threshWhite and dom_color[1] > Config.threshWhite and dom_color[2] > Config.threshWhite:
            return True
        else:
            return False

    def get_segment(x, y, w, h, frame1):
        """
        determines the segment in which the center of the bounding box is
        predefined segments are taken from the .json file specified in Config.segmentPath
        :param x: bounding box x-origin
        :param y: bounding box y-origin
        :param w: width of bounding box
        :param h: height of bounding box
        :return: string of current position
        """
        center = x+w/2, y+h/2
        # for each segment check if center of bounding box is in that segment
        for position in sgmts:
            if center[0] > sgmts[position][0] and center[0] < sgmts[position][1] and center[1] > sgmts[position][2] and center[1] < sgmts[position][3]:
                return position
        return "None"

    def draw_segments(sgmts, frame):
        """
        draw indication of segments into the frame for orientation
        :param sgmts: segment coordinates
        :param frame: current video/stream frame
        :return:
        """
        for sgmt in sgmts:
            cv2.rectangle(frame, pt1=(int(sgmts[sgmt][0]), int(sgmts[sgmt][2])),
                          pt2=(int(sgmts[sgmt][1]), int(sgmts[sgmt][3])),
                          color=(255, 255, 255), thickness=3)

    def overlap_human_bb(x, y, w, h):
        """
        whether given bounding box overlaps with last human bounding box
        :param x: bounding box x-origin
        :param y: bounding box y-origin
        :param w: width of bounding box
        :param h: height of bounding box
        :return: whether there is an overlap
        """
        if (x >= lastHumanBoundingBox[0]+lastHumanBoundingBox[2] or lastHumanBoundingBox[0] >= x+w):
            return False
        if (y >= lastHumanBoundingBox[1]+lastHumanBoundingBox[3] or lastHumanBoundingBox[1] >= y+h):
            return False
        return True

    def doIt(frame1, frame2):
        """

        :param frame1:
        :param frame2:
        :return: whether process was terminated
        """
        ### DETECTION
        diff = cv2.absdiff(frame1, frame2)
        # convert to grey to better get contour
        grey = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
        # blur greyscale frame
        blur = cv2.GaussianBlur(grey, ksize=Config.blur_kernel, sigmaX=0)
        _, thresh = cv2.threshold(src=blur, thresh=Config.diff_threshold, maxval=255, type=cv2.THRESH_BINARY)

        dilated = cv2.dilate(src=thresh, kernel=None, iterations=1)
        #_, contours, _ = cv2.findContours(dilated, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE) #OSX
        contours, _ = cv2.findContours(dilated, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE) #Windows

        ### VISUALIZATION
        cv2.putText(frame1, "Human Position: " + lastPositionHuman.avg(), org=(10, 25), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=1, color=(0, 0, 0), thickness=3)
        cv2.putText(frame1, "Robot Position: " + lastPositionRobot.avg(), org=(10, 55), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=1, color=(0, 0, 0), thickness=3)

        for contour in contours:
            (x, y, w, h) = cv2.boundingRect(contour)
            area = int(cv2.contourArea(contour))

            # could contour be a human
            if area >= Config.human_contour_area:  # change value according to scale of human wrt whole frame

                if not is_robot(frame1, x, y, w, h):

                    # draw bounding box
                    if Config.display:
                        cv2.rectangle(frame1, pt1=(x, y), pt2=(x + w, y + h), color=(0, 255, 0), thickness=2)

                    lastPositionHuman.update(get_segment(x, y, w, h, frame1), int(x+w/2), int(y+h/2))
                    direction = lastPositionHuman.calc_direction()
                    #print(direction)
                    tools.code_direction(direction)

                    if Config.display:
                        cv2.arrowedLine(frame1,pt1=(int(lastPositionHuman.coordinates[-1, 0]), int(lastPositionHuman.coordinates[-1, 1])), pt2=(int(lastPositionHuman.coordinates[-1, 0]) + int(direction[0]), int(lastPositionHuman.coordinates[-1, 1]) + int(direction[1])), color=(0, 0, 255), thickness=2)
                    lastHumanBoundingBox = [x, y, w, h]

                    # agent entered new segment: update
                    global lastPosition
                    if lastPositionHuman.avg() != lastPosition and lastPositionHuman.avg() != "":
                        lastPosition = lastPositionHuman.avg()
                        update_log(lastPosition, (x+w/2, y+h/2))
                        #send_ipaaca_msg(lastPosition, (x+w/2, y+h/2))
                        send_mqtt_msg(lastPosition, (x+w/2, y+h/2))

                # is robot
                else:
                    lastPositionRobot.update(get_segment(x, y, w, h, frame1), int(x + w / 2), int(y + h / 2))
                    # draw bounding box
                    if Config.display:
                        cv2.rectangle(frame1, pt1=(x, y), pt2=(x + w, y + h), color=(0, 0, 255), thickness=2)

            # drop little movementssudo apt install telnet

            else:
                continue

        # WHAT TO DISPLAY
        if Config.display:
        #cv2.drawContours(frame1, contours, -1, color=(255, 0, 0), thickness=2)

            draw_segments(sgmts, frame1)
            #frame1 = cv2.resize(frame1, (960, 540)) # for my laptop
            cv2.imshow('stream', frame1)
            #plt.show()
            #cv2.imshow('blur', blur)
            #cv2.imshow('dilated', dilated)

        if cv2.waitKey(40) == 27:
            return False

        return True

    """
    ########################################################################################################################
    """


    lastPositionHuman = PositionBuffer()
    lastPositionRobot = PositionBuffer()
    # load correct segmentation
    try:
        if Config.segmentPath:
            with open('segments/' + Config.segmentPath,
                      'r') as sgmtFile:
                sgmts = json.load(sgmtFile)
        else:
            with open('segments/' + Config.videoPath.replace("samples/", "").replace(".avi", "") + '.json',
                      'r') as sgmtFile:
                sgmts = json.load(sgmtFile)
    except:
        print("No segmentation found, continue without segments.")
        sgmts = {'None': (0, 0, 0, 0)}

    if Config.videoPath == 'stream':
        cap = cv2.VideoCapture(0)
        ret, frame1 = cap.read()
        ret, frame2 = cap.read()
        print("frame width: ", cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        print("frame height: ", cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

        while doIt(frame1, frame2):
            frame1 = frame2
            ret, frame2 = cap.read()

    # reads video
    else:
        cap = cv2.VideoCapture(Config.videoPath)
        #calibMatrix = np.load('calibration/calibrationMatrix.npz')

        #cap.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
        #cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)

        print("frame width: ", cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        print("frame height: ", cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        ret, frame1 = cap.read()
        ret, frame2 = cap.read()

        counter = 0
        t0 = time.clock()

        while cap.isOpened():
            #frame1 = cv2.remap(frame1,calibMatrix[calibMatrix.files[0]],calibMatrix[calibMatrix.files[1]],cv2.INTER_LINEAR)
            #frame2 = cv2.remap(frame2, calibMatrix[calibMatrix.files[0]], calibMatrix[calibMatrix.files[1]],
            #                   cv2.INTER_LINEAR)
            doIt(frame1, frame2)
            frame1 = frame2
            ret, frame2 = cap.read()
            counter = counter + 1
            if counter == 500:
                t1 = time.clock() - t0
                print("Time for 500 frames: ", t1)
        #cv2.destroyAllWindows()
        cap.release()


        # OSX does not like this one
        #if cv2.getWindowProperty('stream', cv2.WND_PROP_VISIBLE) < 1:
        #    break

    #cv2.destroyAllWindows()
    cap.release()

log.close()
