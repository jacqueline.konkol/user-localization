# user-localization

A tool to track moving objects from a streamed video and determine:
- whether it is the pepper robot (white) or another agent (non-white)
- in which rectangular segment of the frame is the agent (can be defined manually)
- the agent's moving direction


## 1. Dependencies

Python >= 3.6
- numpy
- opencv (cv2)
- json
- datetime


## 2. Installation

On Rasbian:


## 3. Usage

To stream a video, set `videoPath = 'stream'`. Otherwise set to path of prerecorded video.

Adjust default settings to scene, including:
- `blur_kernel = (49, 49)` (size)
- `diff_threshold = 15` sensitivity of pixel difference in order to be seen as motion
- `human_contour_area = 20000` size of moving object to be detected as an agent
- `threshWhite = 220` 0-255, with 255 being perfect white

### Segment creation

Run `segmentCreation.py`, a pop-up window will open showing a still frame.

Drag a rectangle inside the frame and name that rectangle in the console. The rectangle will appear inside the frame. Proceed with arbitrary amount of rectangles.

Pressing `q` inside of the window will terminate programm and a new segmentation file will be stored in `/segments`.

In `Config.py` select desired segmentation by setting `segmentPath = "desiredSegmentation.json"`

### Run localization

`python userLocalization.py`

Automatically saves a logfile in `/logs` containing changes in position with timestamp.

Shows real time preview.

## 4. Troubleshooting

- camera calibration not yet working
- sometimes bounding boxes still overlap