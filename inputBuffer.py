'''
InputBuffer since using ipaaca-iu-sniffer
'''

import ipaaca
import time

print('Start receiving messages.')

incoming_msg_categories = ['UserLocation']
input_buffer = ipaaca.InputBuffer('UserLocationReceiver', incoming_msg_categories)

def iu_handler(iu, event_type, local):
   print("Received category: ", iu.category)
   print("Message payload: ", iu.payload, "\n\n")

input_buffer.register_handler(iu_handler) #This is important. This registers the function that should be called when IUs are received in the input buffer.

#Now let's wait for IUs
while True:
   time.sleep(0.1)

#Note that the order in which the different IUs are received by the receiver is variable. It depends on the scheduling of the threads running in the background.
#If you rerun the receiver and sender, you might notice a different order in which the IUs are processed at the receiver's end.
